﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour
{
    public Camera Camara;
    private float horizontalSpeed = 60;
    private float verticalSpeed = 50;
    private float velocidad = 2.5f;
    private float salto = 6.6f;
    private float gravedad = 9.8f;
    private bool Saltando = false;
    private Vector3 direccion = Vector3.zero;
    public static float ContadorEstrellas = 0;
    public static float ContadorGragea = 0;
    public static float TiempoPartda = 0;
    private Animator Anim;
    public GameObject SpawnBola;
    private int LanzarHechizo = 1;
    public GameObject Spawn;
    public Text MyText;
    public Text MyTiempo;
    public Text Gragea;
    public GameObject lumos;
    public GameObject Mirilla;
    public static float TiempoPartida =0;
    



    void Start()
    {
        Anim = GetComponent<Animator>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        lumos.SetActive(false);
        Mirilla.SetActive(false);
        

    }



    void Update()

    {
        TiempoPartida = Time.timeSinceLevelLoad;
        if (Input.GetMouseButton(0))
        {
            Mirilla.SetActive(true);

        }else if (Input.GetMouseButtonUp(0))
        {
            Mirilla.SetActive(false);
        }


        if (Input.GetKeyDown(KeyCode.LeftControl) && Saltando ==false)
        {
            lumos.SetActive(true);
            Anim.SetBool("Lumos1234",true);
            StartCoroutine(TiempoEspera());


        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            Anim.SetBool("Lumos1234", false);
            
        }

        if (Input.GetKey(KeyCode.RightControl))
        {
            lumos.SetActive(false);
           
        }


        MyTiempo.text = ""+TiempoPartida;
        MyText.text =""+ ContadorEstrellas + "/4";
        if (Input.GetMouseButton(0) && Saltando == false)
        {
            Anim.SetBool("Disparar",true);
            Anim.SetBool("Salto", false);
            LanzarHechizo = 0;
        }
        else if (Input.GetMouseButtonUp(0) )
        {
            Anim.SetBool("Disparar", false);
            LanzarHechizo = 1;
        }
        Vector3 fwd = Camara.transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(Camara.transform.position, fwd * 10, Color.green);
        RaycastHit hit;
        if (Physics.Raycast(Camara.transform.position, fwd, out hit))
        {

            if (hit.collider.gameObject.tag == "Enemigo" && Input.GetMouseButtonDown(0))
            {
               
                Destroy(hit.collider.gameObject);
            }
            
            
            if (hit.collider.gameObject.tag == "Cofre" && Input.GetMouseButtonDown(0))
            {

                
                Cofre XXX = hit.collider.GetComponent<Cofre>();
                XXX.CofreA = true;

               
            }else if (hit.collider.gameObject.tag == "Baldosa" && Input.GetMouseButtonDown(0))
            {
                
                Boton AAA = hit.collider.GetComponent<Boton>();
                AAA.BotonActivado = true;
            }else if (hit.collider.gameObject.tag == "Puerta" && Input.GetMouseButtonDown(0))
            {
                Puerta CCC = hit.collider.GetComponent<Puerta>();
                CCC.PuertaAbrir = true;
               
            }

         
        }
        
        

        
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded )
        {
            direccion = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            direccion = transform.TransformDirection(direccion);
            direccion *= velocidad;
            if (Input.GetButton("Jump"))
            {
                direccion.y = salto;
                Anim.SetBool("Salto",true);
                Saltando = true;
            }else
            {
                Anim.SetBool("Salto", false);
                Saltando = false;
            }
                

        }
        direccion.y -= gravedad * Time.deltaTime;
        
        
            controller.Move(direccion * Time.deltaTime*LanzarHechizo);
        
        

        Anim.SetFloat("Blend", Input.GetAxis("Vertical"));
        Anim.SetFloat("Lados", Input.GetAxis("Horizontal"));
       float h = horizontalSpeed * Input.GetAxisRaw("Mouse X")*Time.deltaTime;
        float v  = verticalSpeed * Input.GetAxisRaw("Mouse Y")* Time.deltaTime;
        
        transform.Rotate(0, h, 0);
        
        Camara.transform.RotateAround(transform.position, transform.right, -v);
    }
    
    void OnControllerColliderHit(ControllerColliderHit hit)
      {

        if (hit.gameObject.tag == "Gragea")
        {
            Destroy(hit.gameObject);
            ContadorGragea = ContadorGragea + 1;
            Gragea.text = "" + ContadorGragea;
        }

        if (hit.gameObject.tag == "Enemigo" )
        {
            
            Muerte();
        }
       
        if (hit.gameObject.tag == "SueloMuerte")
        {
            Muerte();
        }
        if (hit.gameObject.tag == "BaldosaRota")
        {
            Destroy(hit.gameObject);
        }
        if (hit.gameObject.tag == "Estrella")
        {
            Destroy(hit.gameObject);
            ContadorEstrellas = ContadorEstrellas + 1;

        }
        if (hit.gameObject.tag == "EstrellaFinal")
        {
            TiempoPartda = Time.time;
            SceneManager.LoadScene("EscenaFinal");
            Destroy(hit.gameObject);

        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Gragea")
        {
            ContadorGragea = ContadorGragea + 1;
            Destroy(other.gameObject);
            Gragea.text = "" + ContadorGragea;
        }
    }
    public void Muerte()
    {
        transform.position = new Vector3(Spawn.transform.position.x, Spawn.transform.position.y, Spawn.transform.position.z);
    }


    //Corrutinas
    IEnumerator TiempoEspera()
    {
        LanzarHechizo = 0;
        yield return new WaitForSeconds(3.7f);
        LanzarHechizo = 1;
    }

}