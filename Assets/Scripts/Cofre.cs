﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour
{
    public Animator Anim;
    public  bool CofreA = false;
    public GameObject Grageas;
    private int NumeroGrageas = 3;
    private int A = 0;
    public GameObject SpawnGrageas;
    public GameObject SpawnGrageas2;
    public GameObject SpawnGrageas3;


    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CofreA == true)
        {  
            Anim.SetBool("CofreActivo", true);
           
        }
    }
    public void SpawnGragea()
    {
        Instantiate(Grageas, SpawnGrageas.transform.position, Quaternion.identity);
        Instantiate(Grageas, SpawnGrageas2.transform.position, Quaternion.identity);
        Instantiate(Grageas, SpawnGrageas3.transform.position, Quaternion.identity);
    }
}
