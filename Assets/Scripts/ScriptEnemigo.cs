﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class ScriptEnemigo : MonoBehaviour
{
    public static bool PersonajeCerca;
    NavMeshAgent MyAgent;
    public GameObject Personaje;
    public Animator MyAnim;
    // Start is called before the first frame update
    void Start()
    {
        MyAgent = GetComponent<NavMeshAgent>();
        PersonajeCerca = false;
        MyAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PersonajeCerca)
        {
            MyAnim.SetBool("Perseguir",true);
            MyAgent.SetDestination(Personaje.transform.position);
        }else if (PersonajeCerca == false)
        {
            MyAnim.SetBool("Perseguir", false);
        }
    }
    
}
