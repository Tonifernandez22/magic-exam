﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubeBaja : MonoBehaviour
{
    public bool JugadorCerca = true;
    public float VelocidadRampa = 120f;
    public float RotacionInicial;
    private int Direccion = 1;
    public bool Subiendo = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
         if (JugadorCerca)
        {

            
            transform.Rotate(0, -1 * VelocidadRampa * Direccion * Time.deltaTime, 0);

            if (transform.rotation.x > -0.666 && Subiendo==false)
            {
                Direccion = -1;
                
                Subiendo = true;

            }
            
        }
        
    }
    
}
