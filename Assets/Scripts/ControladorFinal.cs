﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControladorFinal : MonoBehaviour
{
    public Text Estre;
    public Text Tiempo;
    public Text Gragea;
    // Start is called before the first frame update
    void Start()
    {
        Gragea.text = Movimiento.ContadorGragea + " Grageas";
        Estre.text = Movimiento.ContadorEstrellas + " Estrellas";
        Tiempo.text ="Tiempo Partida :" + Movimiento.TiempoPartda;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.None;
    }
    public void SalirMenu()
    {
        SceneManager.LoadScene("EscenaMenu");
    }
}
