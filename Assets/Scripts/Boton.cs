﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton : MonoBehaviour
{
    public Animator Anim;
    public  bool BotonActivado=false;
    
    
    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
       
    }

    // Update is called once per frame
    void Update()
    {
        if (BotonActivado == true)
        {
            Anim.SetBool("BaldosaActiva",true);
           
            
        }
    }
}
