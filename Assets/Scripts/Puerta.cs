﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public Animator Anim;
    public bool PuertaAbrir;
    
    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
      
    }

    // Update is called once per frame
    void Update()
    {
        if (PuertaAbrir)
        {

            Anim.SetBool("PuertaAbierta",true);
        }
    }
}
