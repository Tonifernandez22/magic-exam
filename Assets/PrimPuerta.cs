﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PrimPuerta : MonoBehaviour
{
    public Animator MyAnim;
    public static bool BaldosaFinalizado = false;
    // Start is called before the first frame update
    void Start()
    {
        MyAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (BaldosaFinalizado)
        {
            MyAnim.SetBool("PuertaAbierta",true);
        }
    }
}
